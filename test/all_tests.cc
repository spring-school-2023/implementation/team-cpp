#include <gtest/gtest.h>
#include "warehouse_system/warehouse_system.h"
#include "robot.h"
#include "command_parser/command_parser.h"
/*
TEST(RobotTests, GetNameTest) {
  Robot TestRobot("testBot");
  EXPECT_EQ(TestRobot.getRobotName(), "testBot");
}

TEST(RobotTests, SetNameTest) {
  Robot TestRobot("testBot");
  EXPECT_EQ(TestRobot.setRobotName("testRoBot"), 0);
}

TEST(RobotTests, SetBadNameTest) {
  Robot TestRobot("testBot");
  EXPECT_EQ(TestRobot.setRobotName(""), -1);
}

TEST(RobotTests, MoveUpTest) {
  Robot TestRobot("testBot");
  EXPECT_EQ(TestRobot.move("north"), 0);
}

TEST(RobotTests, MoveLeftTest) {
  Robot TestRobot("testBot");
  EXPECT_EQ(TestRobot.move("east"), 0);
}

TEST(RobotTests, MoveRightTest) {
  Robot TestRobot("testBot");
  EXPECT_EQ(TestRobot.move("west"), 0);
}

TEST(RobotTests, MoveDownTest) {
  Robot TestRobot("testBot");
  EXPECT_EQ(TestRobot.move("south"), 0);
}

TEST(RobotTests, MoveWrongTest) {
  Robot TestRobot("testBot");
  EXPECT_EQ(TestRobot.move("center"), -1);
}

TEST(RobotTests, PickUpTest) {
  Robot TestRobot("testBot");
  EXPECT_EQ(TestRobot.pick(), 0);
}

TEST(RobotTests, PlaceTest) {
  Robot TestRobot("testBot");
  EXPECT_EQ(TestRobot.place(), 0);
}

TEST(RobotTests, ScanFarTest) {
  Robot TestRobot("testBot");
  EXPECT_EQ(TestRobot.scanFar(), 0);
}

TEST(RobotTests, ScanNearTest) {
  Robot TestRobot("testBot");
  EXPECT_EQ(TestRobot.scanNear(), 0);
}


TEST(ParserTests, IsValidTest) {
  command_parser TestParser;
  std::vector<std::string> FailedShortTestStrings = {"Alice"};
  std::vector<std::string> FailedLongTestStrings = {"Alice", "move"};
  std::vector<std::string> PassedTestStrings = {"curtis", "move"} ;

  EXPECT_EQ(TestParser.is_valid(FailedShortTestStrings), false);
  EXPECT_EQ(TestParser.is_valid(FailedLongTestStrings), false);
  EXPECT_EQ(TestParser.is_valid(PassedTestStrings), true);
}

TEST(ParserTests, ParsArgsTest) {
  command_parser TestParser;
  char *TestArgs[] = {"IamFileName", "Alice", "Bob"};

  std::vector<std::string> ExpectedTestStrings{"Alice", "Bob"};

  EXPECT_EQ(TestParser.parse_args(3, TestArgs), ExpectedTestStrings);
  EXPECT_NE(TestParser.parse_args(2, TestArgs), ExpectedTestStrings);
}
*/