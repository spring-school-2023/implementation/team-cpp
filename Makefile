.PHONY: setup
setup:
	cmake --no-warn-unused-cli -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING=Debug -DCMAKE_C_COMPILER:FILEPATH=/usr/bin/clang-15 -DCMAKE_CXX_COMPILER:FILEPATH=/usr/bin/clang++-15 -S./ -B./build

.PHONY: build
build: setup
	cmake --build ./build --config Debug --target all --

.PHONY: test
test: build
	./build/spring_school_client_tests --gtest_output="xml:report.xml"

.PHONY: all
all: build test