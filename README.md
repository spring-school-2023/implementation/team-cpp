# Warehouse CLI C++-Template

This is the template project for C++ development in the Festo Spring School. It should provide you a good starting point with some nice features built-in, which we'll explain in more detail as soon as the course starts. So far you'll get some hints here below for the most crucial commands and language specific topics.

- CMake file which includes the proposed Libraries
- Gitlab Pipeline which builds the project and executes the tests

## Setup

You can either use:
- The [GitPod](https://gitpod.io) integration which will spawn a VSCode in browser or you can link your local VSCode to the one Gitpod instance.
- Your own IDE and clone the repository to your local drive

We're making use of `CMake` to build the application and get all the depencies right.

If you happen to use Linux (or Gitpod which is based on Linux) you can also make use of the integrated Makefile which knows the following targets (type them into the console):

- `make setup`: Setup the CMake environment
- `make build`: Compile the application
- `make test`: Run all tests
- `make all`: Setup the environment (if necessary), compile the application and run all test

## Getting Started

We provided you with some basic command line application as a starting point and some tests to show how it could be done. Feel free to implement your own structure. Nevertheless we suggest to keep the existing main folder structure:

- `src`: This is where your application belong into.
- `tests`: Here you'll put the tests

## Libraries

- Rest SDK: https://github.com/microsoft/cpprestsdk [Documentation](https://github.com/microsoft/cpprestsdk/wiki)
- Google Test: https://github.com/google/googletest [Documentation](https://google.github.io/googletest/)