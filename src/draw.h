#ifndef DRAW_H
#define DRAW_H

#include "warehouse/cell.hpp"
#include <vector>
#include <map>
#include <string>

enum en_pattern
{
    PTRN_WALL = 1,
    PTRN_PLUS,
    PTRN_MINUS,
    PTRN_ROBOT
};

class DrawWarehouse
{
    protected:
        int rows;
        int coloms;
        std::vector<std::vector<int>> plot;
        std::map<int, std::string> pattern = 
            {{PTRN_WALL, "|"}, {PTRN_PLUS, "+"}, {PTRN_MINUS, "-"}, {PTRN_ROBOT, "o"}};

    public:
        DrawWarehouse();
        ~DrawWarehouse();
        void draw_row(std::string& pattern, bool endline);
        void prepare_to_draw(std::vector<Cell>& warehouse_vec);
        
};


#endif
