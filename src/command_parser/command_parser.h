#include <stdlib.h>
#include <string>
#include <vector>
#include "robot.h"

class command_parser {
public:
    command_parser();
    ~command_parser() = default;

    std::vector<std::string> parse_args(int num_of_args, char **args) const;
    bool is_valid(const std::vector<std::string>& params) const;
    int execute_bot_command(Robot& r, std::vector<std::string>& command_vec) const;

private:
    std::vector<std::string> valid_robot_names;
    std::vector<std::string> valid_robot_commands;
};
