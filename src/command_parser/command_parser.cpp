#include "command_parser.h"
#include <iostream>
#include <algorithm>
#include "warehouse/warehouse.hpp"

command_parser::command_parser() : valid_robot_names{"curtis", "cliff", "clair", "cheryl", "charles", "carl", "betty", "alex"}
                                 , valid_robot_commands{"move", "pick", "place", "scannear", "scanfar", "status"}
{}

std::vector<std::string> command_parser::parse_args(int num_of_args, char **args) const
{
    std::vector<std::string> args_vector(args + 1, args + num_of_args);

    return args_vector;
}

bool command_parser::is_valid(const std::vector<std::string>& args) const
{
    bool ret = false;

    if(args.size() < 2)
    {
        return ret;
    }

    std::string robot_name = args.front();

    if(std::find(valid_robot_names.begin(), valid_robot_names.end(), robot_name) != valid_robot_names.end()) {
        ret = true;
    } else {
        std::cout << "Invalid robot name!" << std::endl;
        return ret;
    }

    std::string command = args.at(1);

    if(std::find(valid_robot_commands.begin(), valid_robot_commands.end(), command) != valid_robot_commands.end()) {
        ret = true;
    } else {
        std::cout << "Invalid robot command: " << command << std::endl;
        ret = false;
    }

    return ret;
}

int command_parser::execute_bot_command(Robot& r, std::vector<std::string>& command_vec) const
{
   int ret;
    Warehouse simple_warehouse;
    Cell current_cell;
   std::string command = command_vec.at(1);

   if(command == "move")
       ret = r.move(command_vec.at(2));
   else if(command == "pick")
       ret = r.pick();
   else if(command == "place")
       ret = r.place();
   else if(command == "scannear")
       current_cell = r.scanNear();
   else if(command == "scanfar")
       ret = r.scanFar();
   else if(command == "status")
       current_cell = r.status();

    simple_warehouse.add_cell(current_cell);
    std::cout << "Reading out cell: " << std::endl; 
    std::cout << "East wall is: " << current_cell.is_wall_east << std::endl;
    std::cout << "West wall is: " << current_cell.is_wall_west << std::endl;
    std::cout << "North wall is: " << current_cell.is_wall_north << std::endl;
    std::cout << "South wall is: " << current_cell.is_wall_south << std::endl;
    std::cout << "X Coord is: " << current_cell.location.x << std::endl;
    std::cout << "Y Coord is: " << current_cell.location.y << std::endl;

   return ret;


}
