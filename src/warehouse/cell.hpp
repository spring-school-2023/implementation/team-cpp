#ifndef CELL_DEFINITION_HEADER
#define CELL_DEFINITION_HEADER
#include "coordinates.hpp"

class Cell : Coordinates
{    
    public:
    Cell();
    Cell(const Coordinates& location);
    ~Cell();
    
    Coordinates location;

    bool is_wall_south = false;
    bool is_wall_north = false;
    bool is_wall_east = false;
    bool is_wall_west = false;
    bool is_ramp = false;
    bool is_active = false;
    bool is_robot = false;    
};


#endif