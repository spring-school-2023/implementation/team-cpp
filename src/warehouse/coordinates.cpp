#include "warehouse/coordinates.hpp"


Coordinates::Coordinates() 
{
    x = 0;
    y = 0;
}

Coordinates::Coordinates(const int& x, const int& y) : x(x), y(y) {}
Coordinates::~Coordinates() {}

bool Coordinates::operator==(const Coordinates& coords) const
{
    if(this->x == coords.x && this->y == coords.y)
        return true;
    
    return false;
}

Coordinates Coordinates::operator+(const Coordinates& coords) const
{
    Coordinates c;
    c.x = x + coords.x;
    c.y = y + coords.y;    
    return c;
}