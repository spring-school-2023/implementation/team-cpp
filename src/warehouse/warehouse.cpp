#include "warehouse/warehouse.hpp"

Warehouse::Warehouse() {}
Warehouse::~Warehouse() {}

void Warehouse::add_cell(const Cell& cell)
{
    warehouse_layout.push_back(cell);
}

Cell Warehouse::retrieve_cell(Coordinates location)
{
    Cell ret_cell;
    for(Cell single_cell : warehouse_layout)
    {
        if(single_cell.location == location)
        {
            ret_cell = single_cell;
        }
    }
    return ret_cell;
}

Cell Warehouse::retrive_cell(int id)
{
    //if(id < warehouse_layout.size())
    //{    
        //return warehouse_layout[id];
    //}
    return warehouse_layout[id];
}
