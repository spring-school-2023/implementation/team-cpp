#ifndef WAREHOUSE_HEADER_CPP
#define WAREHOUSE_HEADER_CPP
#include "warehouse/cell.hpp"
#include <vector>

class Warehouse
{
    public:
    Warehouse();
    ~Warehouse();
    void add_cell(const Cell& cell);
    //retrive a cell by location
    Cell retrieve_cell(Coordinates location);
    //retreive a cell by id
    Cell retrive_cell(int id);

    private:
    //a storage for all the warehouse cells
    std::vector<Cell> warehouse_layout;

};

#endif