#ifndef COORDINATES_HEADER_HPP
#define COORDINATES_HEADER_HPP

class Coordinates
{
    public:
    Coordinates();
    Coordinates(const int &x, const int &y);
    ~Coordinates();
    bool operator==(const Coordinates& coords)const;
    Coordinates operator+(const Coordinates& coords)const;
    int x;
    int y;
    private:

};

#endif