#include <iostream>
#include <stdio.h>
#include "robot.h"

Robot::Robot(const std::string& robotName)
{
    this->robotName = robotName;
}

std::string& Robot::getRobotName()
{
    return this->robotName;
}

int Robot::setRobotName(const std::string& robotName)
{
    if (robotName == "")
    {
        return -1;
    }

    this->robotName = robotName;

    return 0;
}

int Robot::move(const std::string& moveDirection)
{
    if ((moveDirection == "north") ||
        (moveDirection == "east") ||
        (moveDirection == "west") ||
        (moveDirection == "south"))
    {
        return send_move_command(getRobotName(), moveDirection);
    }

    std::cout << "Error: Wrong or empty parameter" << std::endl;
    return -1;
}

int Robot::pick()
{
    return pick_command(getRobotName());
}

int Robot::place()
{
    return put_command(getRobotName());
}

int Robot::scanFar()
{
    return scan_far_command(getRobotName());
}

Cell Robot::scanNear()
{
    Cell scanned_cell;
    scan_near_command(getRobotName(), scanned_cell);
    return scanned_cell;
}

Cell Robot::status()
{   
    Cell status_cell;
    read_robot_status(getRobotName(), status_cell);
    return status_cell;
}
Robot::~Robot()
{

}
