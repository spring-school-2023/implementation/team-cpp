
#ifndef HTTP_REQ_WRAPPER_HEADER
#define HTTP_REQ_WRAPPER_HEADER
#include <stdbool.h>
#include <string>
#include <cpprest/http_client.h>
#include <cpprest/filestream.h>
#include <cpprest/json.h>
#include "warehouse/cell.hpp"

class HTTP_Req_Wrapper
{
    public:
        HTTP_Req_Wrapper();
        ~HTTP_Req_Wrapper();
        bool send_move_command(const std::string& session_id, const std::string& direction);
        bool read_robot_status(const std::string& session_id, Cell &cell_to_update);
        bool scan_far_command(const std::string& session_id);
        bool scan_near_command(const std::string& session_id, Cell &cell_to_update);
        bool pick_command(const std::string& session_id);
        bool put_command(const std::string& session_id);
    private:
        const std::string BASE_URL = "http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com/api/";

};
#endif
