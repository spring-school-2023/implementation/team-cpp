#include "http_req_wrapper.hpp"

using namespace utility;                    // Common utilities like string conversions
using namespace web;                        // Common features like URIs.
using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features
using namespace web::json;                  // JSON features
using namespace concurrency::streams;       // Asynchronous streams


HTTP_Req_Wrapper::HTTP_Req_Wrapper() {
    //client(U(BASE_URL));
}
HTTP_Req_Wrapper::~HTTP_Req_Wrapper() {}


bool HTTP_Req_Wrapper::send_move_command(const std::string& session_id, const std::string& direction)
{
    // Create an HTTP client
    web::http::client::http_client client(U(BASE_URL));

    // Create an HTTP request
    web::http::http_request request(web::http::methods::PUT);
    uri_builder builder(U("/bot/"+session_id+"/move/"+direction));
    request.set_request_uri(U(builder.to_string()));

    // Send the request and receive the response
    web::http::http_response response = client.request(request).get();

    // Print the response status code and body
    std::cout << "Response status code: " << response.status_code() << std::endl;

    value jsonVal = response.extract_json().get();
    
    // Access the values in the JSON object
    std::string message = jsonVal["message"].as_string();
    std::string request_status = jsonVal["request_status"].as_string();

    // Print the values to the console
    std::cout << "message: " << message << std::endl;
    std::cout << "request_status: " << request_status << std::endl;
    return 0;
}

bool HTTP_Req_Wrapper::read_robot_status(const std::string& session_id, Cell &cell_to_update)
{
    // Create an HTTP client
    web::http::client::http_client client(U(BASE_URL));

    // Create an HTTP request
    web::http::http_request request(web::http::methods::GET);
    uri_builder builder(U("/james/"+session_id+"/locate"));
    request.set_request_uri(U(builder.to_string()));

    // Send the request and receive the response
    web::http::http_response response = client.request(request).get();

    // Print the response status code and body
    std::cout << "Response status code: " << response.status_code() << std::endl;
    value jsonVal = response.extract_json().get();
    object& positionObj = jsonVal["position"].as_object();
    // Access the values in the JSON object
    int x = positionObj["x"].as_integer();
    int y = positionObj["y"].as_integer();
    cell_to_update.location.x = x;
    cell_to_update.location.y = y;
    // Print the values to the console
    std::cout << "x: " << x << std::endl;
    std::cout << "y: " << y << std::endl;

    return 0;
}

bool HTTP_Req_Wrapper::scan_far_command(const std::string& session_id)
{
    // Create an HTTP client
    web::http::client::http_client client(U(BASE_URL));

    // Create an HTTP request
    web::http::http_request request(web::http::methods::GET);
    uri_builder builder(U("/bot/"+session_id+"/scan/far"));
    request.set_request_uri(U(builder.to_string()));

    // Send the request and receive the response
    web::http::http_response response = client.request(request).get();

    // Print the response status code and body
    std::cout << "Response status code: " << response.status_code() << std::endl;
    std::cout << "Response body: " << response.extract_string().get() << std::endl;

    return 0;
}


bool HTTP_Req_Wrapper::scan_near_command(const std::string& session_id, Cell &cell_to_update)
{
    // Create an HTTP client
    web::http::client::http_client client(U(BASE_URL));

    // Create an HTTP request
    web::http::http_request request(web::http::methods::GET);
    uri_builder builder(U("/bot/"+session_id+"/scan/near"));
    request.set_request_uri(U(builder.to_string()));

    // Send the request and receive the response
    web::http::http_response response = client.request(request).get();

    // Print the response status code and body
    value jsonVal = response.extract_json().get();
    
    // Access the values in the JSON object
    object& FieldObj = jsonVal["field_info"].as_object();
    std::string id = FieldObj["id"].as_string();

    int max_capacity = FieldObj["max_capacity"].as_integer();
    object& WallObj = FieldObj["walls"].as_object();
    bool east = WallObj["east"].as_bool();
    bool west = WallObj["west"].as_bool();
    bool north = WallObj["north"].as_bool();
    bool south = WallObj["south"].as_bool();

    cell_to_update.is_wall_east = east;
    cell_to_update.is_wall_west = west;
    cell_to_update.is_wall_north = north;
    cell_to_update.is_wall_south = south;

    // Print the values to the console
    std::cout << "ID: " << id << std::endl;
    std::cout << "max_capacity: " << max_capacity << std::endl;
    std::cout << "east: " << east << std::endl;
    std::cout << "west: " << west << std::endl;
    std::cout << "north: " << north << std::endl;
    std::cout << "south: " << south << std::endl;
    return 0;
}

bool HTTP_Req_Wrapper::pick_command(const std::string& session_id)
{
    // Create an HTTP client
    web::http::client::http_client client(U(BASE_URL));

    // Create an HTTP request
    web::http::http_request request(web::http::methods::PUT);
    uri_builder builder(U("/bot/"+session_id+"/pick"));
    request.set_request_uri(U(builder.to_string()));

    // Send the request and receive the response
    web::http::http_response response = client.request(request).get();

    // Print the response status code and body
    std::cout << "Response status code: " << response.status_code() << std::endl;
    std::cout << "Response body: " << response.extract_string().get() << std::endl;

    return 0;
}


bool HTTP_Req_Wrapper::put_command(const std::string& session_id)
{
    // Create an HTTP client
    web::http::client::http_client client(U(BASE_URL));

    // Create an HTTP request
    web::http::http_request request(web::http::methods::PUT);
    uri_builder builder(U("/bot/"+session_id+"/place"));
    request.set_request_uri(U(builder.to_string()));

    // Send the request and receive the response
    web::http::http_response response = client.request(request).get();

    // Print the response status code and body
    std::cout << "Response status code: " << response.status_code() << std::endl;
    std::cout << "Response body: " << response.extract_string().get() << std::endl;

    return 0;
}
