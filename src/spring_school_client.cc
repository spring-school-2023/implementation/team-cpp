#include <iostream>
#include "command_parser/command_parser.h"

#include "warehouse_system/warehouse_system.h"
#include "robot.h"
#include "warehouse/cell.hpp"
#include "warehouse/coordinates.hpp"
#include "warehouse/warehouse.hpp"

int main(int argc, char** argv)
{
    Cell c(Coordinates(1, 2));
    std::cout << "Coordinates are: " << c.location.x << c.location.y << std::endl;
    Cell c1(Coordinates(2,4));
    Warehouse w;


    command_parser cmd_parser;
    std::vector<std::string> vect = cmd_parser.parse_args(argc, argv);
    auto valid = cmd_parser.is_valid(vect);
    if(valid == false)
    {
        std::cout << "Error: command line parameters!" << std::endl;
        return 0;
    }

    Robot target_robot(vect.front());
    int retError = cmd_parser.execute_bot_command(target_robot, vect);

    if (retError != 0)
    {
        std::cout << "Error: Returned value: " << retError << std::endl;
        return retError;
    }

    return 0;
}
