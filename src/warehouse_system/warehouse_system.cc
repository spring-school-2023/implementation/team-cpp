#include <cpprest/http_client.h>
#include <cpprest/filestream.h>

/// Example function for the testcase in test/examples/spring_school_example_tests.cc.
/// It always returns true.
bool example_function_under_test(){
    return true;
}

std::string return_hello_world() {
    return "Hello World!";
}
