#include <iostream>
#include "draw.h"
#include <algorithm>

DrawWarehouse::DrawWarehouse()
{

}

void DrawWarehouse::draw_row(std::string& pattern, bool endline)
{
    std::cout << pattern;
}

void DrawWarehouse::prepare_to_draw(std::vector<Cell>& warehouse_vec)
{
    std::sort(warehouse_vec.begin(), warehouse_vec.end(), [](Cell& left, Cell& right)
                                                            {
                                                                if(left.location.y > right.location.y)
                                                                    return true;
                                                                else
                                                                    return left.location.x < right.location.y;
                                                            });

    for(auto& point : warehouse_vec)
    {
        std::cout << std::endl;
    }
}
