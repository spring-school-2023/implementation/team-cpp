#ifndef ROBOT_H
#define ROBOT_H

#include <string>
#include "http_req_wrapper/http_req_wrapper.hpp"
#include "warehouse/cell.hpp"

class Robot : HTTP_Req_Wrapper {
private:
    std::string robotName;
public:
    Robot(const std::string& robotName);
    ~Robot();
    std::string& getRobotName();
    int setRobotName(const std::string& robotName);

    /* Robot commands */
    int move(const std::string& moveDirection);
    Cell status();
    int pick();
    int place();
    int scanFar();
    Cell scanNear();

};

#endif
